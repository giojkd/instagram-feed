<?php

namespace App\Http\Controllers;

use App\Models\Token;
use Dymantic\InstagramFeed\Profile;
use EspressoDev\InstagramBasicDisplay\InstagramBasicDisplay;
use Illuminate\Support\Facades\Log;

use Illuminate\Http\Request;

class FrontController extends Controller
{
    //

    public function getInstagram(){
        return
        new InstagramBasicDisplay([
            'appId' => env('INSTAGRAM_APP_ID'),
            'appSecret' => env('INSTAGRAM_APP_SECRET'),
            'redirectUri' => env('INSTAGRAM_REDIRECT_URL')
        ]);
    }

    public function getInstagramAuthUrl(){
        $instagram = $this->getInstagram();
        return redirect($instagram->getLoginUrl());
        #echo "<a href='{}'>Login with Instagram</a>";
        #$profile = new Profile();
        #return $profile->getInstagramAuthUrl();
    }

    public function instagramCallback(Request $request)
    {
        $instagram = $this->getInstagram();
        $token = $instagram->getOAuthToken($request->code, true);
        $token = $instagram->getLongLivedToken($token, true);
        $instagram->setAccessToken($token);
        $profile = $instagram->getUserProfile();
        $token = Token::updateOrCreate(['username'=>$profile->username],['token'=>$token,'account_type'=>$profile->account_type,
        'account_id'=>$profile->id,'media_count'=>$profile->media_count]);
        $tokens = Token::get();
        return $tokens;
    }

    public function refreshToken(){}

    public function getFeed(Request $request){

        if(!$request->username){
            return ['status'=>'error','message'=> 'pass username as querystring parameter'];
        }

        $limit = ($request->has('limit')) ? $request->limit : 99;
        $token = Token::where('username',$request->username)->orderBy('id','DESC')->firstOrFail();



        $token = $token->token;



        $instagram = $this->getInstagram();
        $instagram->setAccessToken($token);

        $profile = $instagram->getUserProfile();

        $media = $instagram->getUserMedia('me', $limit);
        $pages = $instagram->pagination($media);
        #dd($pages);
        #dd($media);
        return $media->data;
        return view('feed',['posts' => $media->data]);

    }
}
