@php
    #dd($posts);
@endphp

@foreach ($posts as $post)

    <div>
        @if ($post->media_type == 'IMAGE')
                <img src="{{ $post->media_url }}" height="320" alt="">
        @endif
        @if ($post->media_type == 'CAROUSEL_ALBUM')
            <img src="{{ $post->media_url }}" height="320" alt="">
            @foreach ($post->children->data as $postChild)
                <img src="{{ $postChild->media_url }}" height="180" alt="">
            @endforeach
        @endif
        @if ($post->media_type == 'VIDEO')
            <video controls>
                <source src="{{ $post->media_url }}">
            </video>
        @endif
        @if (isset($post->caption))
            <p>{!! $post->caption !!}</p>
        @endif
         <a target="blank" href="{{ $post->permalink }}">Link</a>
    </div>

@endforeach
