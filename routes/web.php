<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('auth', '\App\Http\Controllers\FrontController@getInstagramAuthUrl');


Route::get('instagram-auth-failure', '\App\Http\Controllers\FrontController@instagramAuthFailure');
Route::get('instagram/auth/callback/', '\App\Http\Controllers\FrontController@instagramCallback');

